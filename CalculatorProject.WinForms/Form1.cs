﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CalculatorProject.WinForms
{
    public partial class Form1 : Form
    {
        private bool CanSecondWrite => !string.IsNullOrEmpty(mathSign.Text);

        private bool CanCalculate =>
            !string.IsNullOrEmpty(firstNumber.Text) && !string.IsNullOrEmpty(secondNumber.Text);

        private bool CanCalculateByOne => !string.IsNullOrEmpty(firstNumber.Text);


        public Form1()
        {
            InitializeComponent();
        }

        #region События для ввода числа

        private void num1_Click(object sender, EventArgs e)
        {
            if (CanSecondWrite) WriteSecond("1");
            else WriteFirst("1");
        }

        private void num2_Click(object sender, EventArgs e)
        {
            if (CanSecondWrite) WriteSecond("2");
            else WriteFirst("2");
        }

        private void num3_Click(object sender, EventArgs e)
        {
            if (CanSecondWrite) WriteSecond("3");
            else WriteFirst("3");
        }

        private void num4_Click(object sender, EventArgs e)
        {
            if (CanSecondWrite) WriteSecond("4");
            else WriteFirst("4");
        }

        private void num5_Click(object sender, EventArgs e)
        {
            if (CanSecondWrite) WriteSecond("5");
            else WriteFirst("5");
        }

        private void num6_Click(object sender, EventArgs e)
        {
            if (CanSecondWrite) WriteSecond("6");
            else WriteFirst("6");
        }

        private void num7_Click(object sender, EventArgs e)
        {
            if (CanSecondWrite) WriteSecond("7");
            else WriteFirst("7");
        }

        private void num8_Click(object sender, EventArgs e)
        {
            if (CanSecondWrite) WriteSecond("8");
            else WriteFirst("8");
        }

        private void num9_Click(object sender, EventArgs e)
        {
            if (CanSecondWrite) WriteSecond("9");
            else WriteFirst("9");
        }

        private void num0_Click(object sender, EventArgs e)
        {
            if (CanSecondWrite) WriteSecond("0");
            else WriteFirst("0");
        }

        private void numComma_Click(object sender, EventArgs e)
        {
            if (CanSecondWrite)
            {
                if (string.IsNullOrEmpty(secondNumber.Text) || secondNumber.Text.IndexOf(',') != -1)
                    return;
                WriteSecond(",");
            }
            else
            {
                if (string.IsNullOrEmpty(firstNumber.Text) || firstNumber.Text.IndexOf(',') != -1)
                    return;
                WriteFirst(",");
            }
        }

        private void WriteFirst(string s)
        {
            firstNumber.Text += s;
        }

        private void WriteSecond(string s)
        {
            secondNumber.Text += s;
        }

        #endregion

        #region нажатие на знак математический

        private void signPlus_Click(object sender, EventArgs e)
        {
            mathSign.Text = "+";
        }

        private void signMinus_Click(object sender, EventArgs e)
        {
            mathSign.Text = "-";
        }

        private void signMultiply_Click(object sender, EventArgs e)
        {
            mathSign.Text = "*";
        }

        private void signDivide_Click(object sender, EventArgs e)
        {
            mathSign.Text = "/";
        }

        #endregion


        private void actionResult_Click(object sender, EventArgs e)
        {
            if (CanCalculate)
            {
                result.Text = Calculate().ToString(CultureInfo.InvariantCulture);
            }
        }

        private double Calculate()
        {
            var first = Convert.ToDouble(firstNumber.Text);
            var second = Convert.ToDouble(secondNumber.Text);
            switch (mathSign.Text)
            {
                case "+":
                    return first + second;
                case "-":
                    return first - second;
                case "*":
                    return first * second;
                case "/":
                    return first / second;
                default:
                    return 0;
            }
        }

        private void actionRemove_Click(object sender, EventArgs e)
        {
            firstNumber.Text = "";
            secondNumber.Text = "";
            result.Text = "";
            mathSign.Text = "";
        }

        private void actionPower_Click(object sender, EventArgs e)
        {
            if (CanCalculateByOne)
            {
                result.Text = Math.Pow(Convert.ToDouble(firstNumber.Text), 2).ToString(CultureInfo.InvariantCulture);
            }
        }

        private void actionSqrt_Click(object sender, EventArgs e)
        {
            if (CanCalculateByOne)
            {
                result.Text = Math.Sqrt(Convert.ToDouble(firstNumber.Text)).ToString(CultureInfo.InvariantCulture);
            }
        }
    }
}