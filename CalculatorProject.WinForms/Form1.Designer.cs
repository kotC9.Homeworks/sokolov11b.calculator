﻿namespace CalculatorProject.WinForms
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mathSign = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.firstNumber = new System.Windows.Forms.Label();
            this.secondNumber = new System.Windows.Forms.Label();
            this.result = new System.Windows.Forms.Label();
            this.num1 = new System.Windows.Forms.Button();
            this.num2 = new System.Windows.Forms.Button();
            this.num3 = new System.Windows.Forms.Button();
            this.signMultiply = new System.Windows.Forms.Button();
            this.signMinus = new System.Windows.Forms.Button();
            this.signPlus = new System.Windows.Forms.Button();
            this.signDivide = new System.Windows.Forms.Button();
            this.num9 = new System.Windows.Forms.Button();
            this.num8 = new System.Windows.Forms.Button();
            this.num7 = new System.Windows.Forms.Button();
            this.num6 = new System.Windows.Forms.Button();
            this.num5 = new System.Windows.Forms.Button();
            this.num4 = new System.Windows.Forms.Button();
            this.numComma = new System.Windows.Forms.Button();
            this.num0 = new System.Windows.Forms.Button();
            this.actionRemove = new System.Windows.Forms.Button();
            this.actionResult = new System.Windows.Forms.Button();
            this.actionPower = new System.Windows.Forms.Button();
            this.actionSqrt = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // mathSign
            // 
            this.mathSign.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.mathSign.Location = new System.Drawing.Point(56, 42);
            this.mathSign.Name = "mathSign";
            this.mathSign.Size = new System.Drawing.Size(40, 23);
            this.mathSign.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.label4.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label4.Location = new System.Drawing.Point(222, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 23);
            this.label4.TabIndex = 3;
            this.label4.Text = "=";
            // 
            // firstNumber
            // 
            this.firstNumber.BackColor = System.Drawing.SystemColors.ControlLight;
            this.firstNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.firstNumber.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.firstNumber.Location = new System.Drawing.Point(102, 22);
            this.firstNumber.Name = "firstNumber";
            this.firstNumber.Size = new System.Drawing.Size(100, 23);
            this.firstNumber.TabIndex = 1;
            // 
            // secondNumber
            // 
            this.secondNumber.BackColor = System.Drawing.SystemColors.ControlLight;
            this.secondNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.secondNumber.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.secondNumber.Location = new System.Drawing.Point(102, 64);
            this.secondNumber.Name = "secondNumber";
            this.secondNumber.Size = new System.Drawing.Size(100, 23);
            this.secondNumber.TabIndex = 2;
            // 
            // result
            // 
            this.result.BackColor = System.Drawing.SystemColors.ControlLight;
            this.result.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.result.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.result.Location = new System.Drawing.Point(266, 48);
            this.result.Name = "result";
            this.result.Size = new System.Drawing.Size(100, 23);
            this.result.TabIndex = 4;
            this.result.Text = "Результат";
            this.result.Click += new System.EventHandler(this.actionResult_Click);
            // 
            // num1
            // 
            this.num1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.num1.Location = new System.Drawing.Point(109, 207);
            this.num1.Name = "num1";
            this.num1.Size = new System.Drawing.Size(35, 35);
            this.num1.TabIndex = 5;
            this.num1.Text = "1";
            this.num1.UseVisualStyleBackColor = true;
            this.num1.Click += new System.EventHandler(this.num1_Click);
            // 
            // num2
            // 
            this.num2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.num2.Location = new System.Drawing.Point(150, 207);
            this.num2.Name = "num2";
            this.num2.Size = new System.Drawing.Size(35, 35);
            this.num2.TabIndex = 6;
            this.num2.Text = "2";
            this.num2.UseVisualStyleBackColor = true;
            this.num2.Click += new System.EventHandler(this.num2_Click);
            // 
            // num3
            // 
            this.num3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.num3.Location = new System.Drawing.Point(191, 207);
            this.num3.Name = "num3";
            this.num3.Size = new System.Drawing.Size(35, 35);
            this.num3.TabIndex = 7;
            this.num3.Text = "3";
            this.num3.UseVisualStyleBackColor = true;
            this.num3.Click += new System.EventHandler(this.num3_Click);
            // 
            // signMultiply
            // 
            this.signMultiply.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.signMultiply.Location = new System.Drawing.Point(232, 207);
            this.signMultiply.Name = "signMultiply";
            this.signMultiply.Size = new System.Drawing.Size(35, 35);
            this.signMultiply.TabIndex = 8;
            this.signMultiply.Text = "*";
            this.signMultiply.UseVisualStyleBackColor = true;
            this.signMultiply.Click += new System.EventHandler(this.signMultiply_Click);
            // 
            // signMinus
            // 
            this.signMinus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.signMinus.Location = new System.Drawing.Point(232, 166);
            this.signMinus.Name = "signMinus";
            this.signMinus.Size = new System.Drawing.Size(35, 35);
            this.signMinus.TabIndex = 9;
            this.signMinus.Text = "-";
            this.signMinus.UseVisualStyleBackColor = true;
            this.signMinus.Click += new System.EventHandler(this.signMinus_Click);
            // 
            // signPlus
            // 
            this.signPlus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.signPlus.Location = new System.Drawing.Point(232, 125);
            this.signPlus.Name = "signPlus";
            this.signPlus.Size = new System.Drawing.Size(35, 35);
            this.signPlus.TabIndex = 10;
            this.signPlus.Text = "+";
            this.signPlus.UseVisualStyleBackColor = true;
            this.signPlus.Click += new System.EventHandler(this.signPlus_Click);
            // 
            // signDivide
            // 
            this.signDivide.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.signDivide.Location = new System.Drawing.Point(232, 248);
            this.signDivide.Name = "signDivide";
            this.signDivide.Size = new System.Drawing.Size(35, 35);
            this.signDivide.TabIndex = 11;
            this.signDivide.Text = "/";
            this.signDivide.UseVisualStyleBackColor = true;
            this.signDivide.Click += new System.EventHandler(this.signDivide_Click);
            // 
            // num9
            // 
            this.num9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.num9.Location = new System.Drawing.Point(191, 125);
            this.num9.Name = "num9";
            this.num9.Size = new System.Drawing.Size(35, 35);
            this.num9.TabIndex = 14;
            this.num9.Text = "9";
            this.num9.UseVisualStyleBackColor = true;
            this.num9.Click += new System.EventHandler(this.num9_Click);
            // 
            // num8
            // 
            this.num8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.num8.Location = new System.Drawing.Point(150, 125);
            this.num8.Name = "num8";
            this.num8.Size = new System.Drawing.Size(35, 35);
            this.num8.TabIndex = 13;
            this.num8.Text = "8";
            this.num8.UseVisualStyleBackColor = true;
            this.num8.Click += new System.EventHandler(this.num8_Click);
            // 
            // num7
            // 
            this.num7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.num7.Location = new System.Drawing.Point(109, 125);
            this.num7.Name = "num7";
            this.num7.Size = new System.Drawing.Size(35, 35);
            this.num7.TabIndex = 12;
            this.num7.Text = "7";
            this.num7.UseVisualStyleBackColor = true;
            this.num7.Click += new System.EventHandler(this.num7_Click);
            // 
            // num6
            // 
            this.num6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.num6.Location = new System.Drawing.Point(191, 166);
            this.num6.Name = "num6";
            this.num6.Size = new System.Drawing.Size(35, 35);
            this.num6.TabIndex = 17;
            this.num6.Text = "6";
            this.num6.UseVisualStyleBackColor = true;
            this.num6.Click += new System.EventHandler(this.num6_Click);
            // 
            // num5
            // 
            this.num5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.num5.Location = new System.Drawing.Point(150, 166);
            this.num5.Name = "num5";
            this.num5.Size = new System.Drawing.Size(35, 35);
            this.num5.TabIndex = 16;
            this.num5.Text = "5";
            this.num5.UseVisualStyleBackColor = true;
            this.num5.Click += new System.EventHandler(this.num5_Click);
            // 
            // num4
            // 
            this.num4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.num4.Location = new System.Drawing.Point(109, 166);
            this.num4.Name = "num4";
            this.num4.Size = new System.Drawing.Size(35, 35);
            this.num4.TabIndex = 15;
            this.num4.Text = "4";
            this.num4.UseVisualStyleBackColor = true;
            this.num4.Click += new System.EventHandler(this.num4_Click);
            // 
            // numComma
            // 
            this.numComma.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.numComma.Location = new System.Drawing.Point(191, 248);
            this.numComma.Name = "numComma";
            this.numComma.Size = new System.Drawing.Size(35, 35);
            this.numComma.TabIndex = 18;
            this.numComma.Text = ",";
            this.numComma.UseVisualStyleBackColor = true;
            this.numComma.Click += new System.EventHandler(this.numComma_Click);
            // 
            // num0
            // 
            this.num0.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.num0.Location = new System.Drawing.Point(109, 248);
            this.num0.Name = "num0";
            this.num0.Size = new System.Drawing.Size(76, 35);
            this.num0.TabIndex = 19;
            this.num0.Text = "0";
            this.num0.UseVisualStyleBackColor = true;
            this.num0.Click += new System.EventHandler(this.num0_Click);
            // 
            // actionRemove
            // 
            this.actionRemove.BackColor = System.Drawing.Color.FromArgb(((int) (((byte) (192)))), ((int) (((byte) (255)))), ((int) (((byte) (192)))));
            this.actionRemove.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.actionRemove.Location = new System.Drawing.Point(273, 207);
            this.actionRemove.Name = "actionRemove";
            this.actionRemove.Size = new System.Drawing.Size(35, 76);
            this.actionRemove.TabIndex = 20;
            this.actionRemove.Text = "C";
            this.actionRemove.UseVisualStyleBackColor = false;
            this.actionRemove.Click += new System.EventHandler(this.actionRemove_Click);
            // 
            // actionResult
            // 
            this.actionResult.BackColor = System.Drawing.Color.FromArgb(((int) (((byte) (192)))), ((int) (((byte) (255)))), ((int) (((byte) (192)))));
            this.actionResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.actionResult.Location = new System.Drawing.Point(273, 125);
            this.actionResult.Name = "actionResult";
            this.actionResult.Size = new System.Drawing.Size(35, 76);
            this.actionResult.TabIndex = 21;
            this.actionResult.Text = "=";
            this.actionResult.UseVisualStyleBackColor = false;
            this.actionResult.Click += new System.EventHandler(this.actionResult_Click);
            // 
            // actionPower
            // 
            this.actionPower.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.actionPower.Location = new System.Drawing.Point(314, 125);
            this.actionPower.Name = "actionPower";
            this.actionPower.Size = new System.Drawing.Size(35, 76);
            this.actionPower.TabIndex = 23;
            this.actionPower.Text = "x²";
            this.actionPower.UseVisualStyleBackColor = true;
            this.actionPower.Click += new System.EventHandler(this.actionPower_Click);
            // 
            // actionSqrt
            // 
            this.actionSqrt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.actionSqrt.Location = new System.Drawing.Point(314, 207);
            this.actionSqrt.Name = "actionSqrt";
            this.actionSqrt.Size = new System.Drawing.Size(35, 76);
            this.actionSqrt.TabIndex = 22;
            this.actionSqrt.Text = "√";
            this.actionSqrt.UseVisualStyleBackColor = true;
            this.actionSqrt.Click += new System.EventHandler(this.actionSqrt_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.actionPower);
            this.Controls.Add(this.actionSqrt);
            this.Controls.Add(this.actionResult);
            this.Controls.Add(this.actionRemove);
            this.Controls.Add(this.num0);
            this.Controls.Add(this.numComma);
            this.Controls.Add(this.num6);
            this.Controls.Add(this.num5);
            this.Controls.Add(this.num4);
            this.Controls.Add(this.num9);
            this.Controls.Add(this.num8);
            this.Controls.Add(this.num7);
            this.Controls.Add(this.signDivide);
            this.Controls.Add(this.signPlus);
            this.Controls.Add(this.signMinus);
            this.Controls.Add(this.signMultiply);
            this.Controls.Add(this.num3);
            this.Controls.Add(this.num2);
            this.Controls.Add(this.num1);
            this.Controls.Add(this.result);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.secondNumber);
            this.Controls.Add(this.firstNumber);
            this.Controls.Add(this.mathSign);
            this.Location = new System.Drawing.Point(15, 15);
            this.Name = "Form1";
            this.ResumeLayout(false);
        }

        private System.Windows.Forms.Button actionPower;
        private System.Windows.Forms.Button actionRemove;
        private System.Windows.Forms.Button actionResult;
        private System.Windows.Forms.Button actionSqrt;

        private System.Windows.Forms.Button num0;
        private System.Windows.Forms.Button numComma;

        private System.Windows.Forms.Button signDivide;

        private System.Windows.Forms.Button signMultiply;

        private System.Windows.Forms.Button num6;
        private System.Windows.Forms.Button num7;
        private System.Windows.Forms.Button num8;
        private System.Windows.Forms.Button num9;
        private System.Windows.Forms.Button signMinus;
        private System.Windows.Forms.Button signPlus;

        private System.Windows.Forms.Button num1;
        private System.Windows.Forms.Button num2;
        private System.Windows.Forms.Button num3;
        private System.Windows.Forms.Button num4;
        private System.Windows.Forms.Button num5;
        private System.Windows.Forms.Label result;

        private System.Windows.Forms.Label firstNumber;
        private System.Windows.Forms.Label secondNumber;

        private System.Windows.Forms.Label mathSign;

        private System.Windows.Forms.Label label4;

        #endregion
    }
}